import React, { createContext } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Panel from './pages/Panel';
import Nav from './components/Nav';
import AddDisk from './pages/AddDisk';
import uniqid from 'uniqid';
import DiskPage from './pages/DiskPage';


const DiskContext = createContext();

class App extends React.Component {

  state = {
    disks: [],
    priceHDD: [
      {
        size: "2",
        price: 1
      },
      {
        size: "4",
        price: 2
      },
      {
        size: "8",
        price: 4
      },
      {
        size: "16",
        price: 8
      },
      {
        size: "32",
        price: 16
      },
      {
        size: "64",
        price: 32
      }
    ],
    priceSSD: [
      {
        size: "2",
        price: 4
      },
      {
        size: "4",
        price: 6
      },
      {
        size: "8",
        price: 8
      },
      {
        size: "16",
        price: 12
      },
      {
        size: "32",
        price: 20
      },
      {
        size: "64",
        price: 37
      }
    ],
    number: uniqid(),
    monthlyCost: 0,
  }

  componentDidMount = () => {
    fetch("http://localhost:3000/disks").then(req => {
      return req.json();
    }).then(disks => {

      this.setState({
        disks,
        monthlyCost: this.changeMonthlyCost(disks)
      })
    })
  }

  addNewDisk = (size, type) => {
    let priceDisk = 0;

    priceDisk = this.getPriceDisk(size, type);

    const disk = {
      number: uniqid(),
      size: size,
      type: type,
      use: 0,
      price: priceDisk,
      users: []
    }

    fetch("http://localhost:3000/disks", {
      method: "POST",
      body: JSON.stringify(
        {
          number: disk.number,
          size: disk.size,
          type: disk.type,
          use: disk.use,
          price: disk.price,
          users: []
        }
      ),
      headers: {
        "Content-Type": "application/json"
      }
    }).then((req) => {
      return req.json();
    }).then(disk => {
      const disks = this.state.disks;

      disks.push(disk);
      this.setState({
        disks,
        monthlyCost: this.changeMonthlyCost(disks)
      })
    })

    return true;
  }

  deleteDisk = (id) => {
    let disks = [...this.state.disks];

    fetch("http://localhost:3000/disks/" + id, {
      method: "DELETE"
    }).then(() => {
      disks = disks.filter(disk => id !== disk.id);

      this.setState({
        disks,
        monthlyCost: this.changeMonthlyCost(disks)
      })
    })
  }

  changeMonthlyCost = (disks) => {
    let cost = 0;
    for (let i = 0; i < disks.length; i++) {
      cost += disks[i].price;
    }
    return cost;
  }

  getPriceDisk = (size, type) => {
    let priceDisk = 0;
    if (type === "HDD") {
      this.state.priceHDD.forEach(hdd => {
        if (hdd.size === size) {
          priceDisk = hdd.price
        }
      });
    } else {
      this.state.priceSSD.forEach(ssd => {
        if (ssd.size === size) {
          priceDisk = ssd.price
        }
      });
    }

    return priceDisk;
  }

  changeUserPermission = (id, email, name, value) => {

    let disks = [...this.state.disks];

    let disk = disks.filter(disk => (
      disk.id === id
    ))

    let users = disk[0].users;

    for (let i = 0; i < users.length; i++) {
      if (users[i].email === email) {
        users[i][name] = value;
      }
    }

    fetch("http://localhost:3000/disks/" + id, {
      method: "PATCH",
      body: JSON.stringify({
        users
      }),
      headers: {
        "Content-Type": "application/json"
      }
    }).then(() => {
      this.setState({
        disks
      })
    })

  }

  deleteUser = (id, email) => {
    let disks = [...this.state.disks];

    let disk = disks.filter(disk => (
      disk.id === id
    ))

    for (let i = 0; i < disk[0].users.length; i++) {
      if (disk[0].users[i].email === email) {
        disk[0].users.splice(i, 1);
      }
    }

    let users = disk[0].users;

    fetch("http://localhost:3000/disks/" + id, {
      method: "PATCH",
      body: JSON.stringify({
        users
      }),
      headers: {
        "Content-Type": "application/json"
      }
    }).then(() => {
      this.setState({
        disks
      })
    })

  }

  addNewUser = (id, email, read, write, del) => {
    const disks = [...this.state.disks];

    let disk = disks.filter(disk => (
      disk.id === id
    ))

    let users = disk[0].users;

    const user = {
      email,
      read,
      write,
      del
    }

    users.push(user);

    fetch("http://localhost:3000/disks/" + id, {
      method: "PATCH",
      body: JSON.stringify({
        users
      }),
      headers: {
        "Content-Type": "application/json"
      }
    }).then(() => {
      this.setState({
        disks
      })
    })

    return true;
  }


  render() {
    return (
      <DiskContext.Provider
        value={{ disks: this.state.disks, addNewDisk: this.addNewDisk, deleteDisk: this.deleteDisk, monthlyCost: this.state.monthlyCost, getPriceDisk: this.getPriceDisk, changeUserPermission: this.changeUserPermission, deleteUser: this.deleteUser, addNewUser: this.addNewUser }}
      >
        <div>
          <BrowserRouter>
            <Nav />
            <Switch>
              <Route exact path="/" component={Panel} />
              <Route path="/add-disk" component={AddDisk} />
              <Route path="/disk-page/:id" component={DiskPage} />
            </Switch>
          </BrowserRouter>
        </div>
      </DiskContext.Provider>
    )
  }
}

export default App;
export { DiskContext };