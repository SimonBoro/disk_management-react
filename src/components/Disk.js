import React from 'react';
import '../style/Disk.css';

const Disk = (props) => {
    const { id, number, size, type, use, price } = props.disk;

    return (
        <div className="disk">
            <img src={type === "HDD" ? "/img/hdd.svg" : "/img/ssd.svg"} alt="disk" className="disk-img" />
            <div>
                <div className="parameters">
                    <div className="parameters1">
                        <h3>Disk Number: {number}</h3>
                        <p>Size: {size}</p>
                        <p>Type: {type}</p>
                        <p>Usage: {use < 1 ? use * 1000 + "MB/" + size + "GB" : use + "GB/" + size + "GB"}</p>
                    </div>
                    <div className="parameters2">
                        <p>Price: {price}$</p>
                        <img src="/img/trash.svg" alt="bin" className="trash" onClick={() => props.deleteDisk(id, props.diskPage)} />
                    </div>
                </div>
                <div className="options">
                    <p className="bar-of-use" style={{ background: `linear-gradient(90deg, rgba(0,0,255,1) ${use / size * 100}%, rgba(255,255,255,1) ${use / size * 100}%)` }}></p>
                    {props.diskPage === false ? <button onClick={() => props.props.history.push("/disk-page/" + id)}>Disk Page</button> : null}
                </div>
            </div>
        </div>
    );
}

export default Disk;