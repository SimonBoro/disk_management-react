import React, { useContext } from 'react';
import { DiskContext } from '../App';
import '../style/Nav.css';

const Nav = (props) => {

    const diskContext = useContext(DiskContext)

    return (
        <div className="nav">
            <button>Billing Panel</button>
            <h3>Monthly costs: {diskContext.monthlyCost}$</h3>
        </div>
    );
}

export default Nav;