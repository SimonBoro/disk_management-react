import React from 'react';
import '../style/User.css';


const Users = (props) => {
    const { email, read, write, del } = props.user;

    return (
        <tr>
            <td>{email}</td>
            <td><input type="checkbox" name="read" checked={read} onChange={(e) => props.changeUserPermission(props.diskId, email, e.target.name, e.target.checked)} /></td>
            <td><input type="checkbox" name="write" checked={write} onChange={(e) => props.changeUserPermission(props.diskId, email, e.target.name, e.target.checked)} /></td>
            <td><input type="checkbox" name="del" checked={del} onChange={(e) => props.changeUserPermission(props.diskId, email, e.target.name, e.target.checked)} /></td>
            <td><button onClick={() => props.deleteUser(props.diskId, email)}>Delete</button></td>
        </tr>
    );
}

export default Users;