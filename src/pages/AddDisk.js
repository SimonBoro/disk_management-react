import React from 'react';
import '../style/AddDisk.css';
import { DiskContext } from '../App';

class AddDisk extends React.Component {
    state = {
        size: "2",
        type: "HDD",
        priceDisk: 0
    }

    inputChange = (name, value, getPriceDisk) => {
        let priceDisk = this.state.priceDisk;
        const { size, type } = this.state;

        if (name === "type") {
            priceDisk = getPriceDisk(size, value);
        } else if (name === "size") {
            priceDisk = getPriceDisk(value, type);
        }

        this.setState({
            [name]: value,
            priceDisk
        })
    }

    addDiskButton = (addNewDisk) => {
        const { size, type } = this.state;

        const add = addNewDisk(size, type);

        if (add) {
            this.setState({
                size: "2",
                type: "HDD",
                priceDisk: 0
            })
        }
    }

    render() {
        return (
            <DiskContext.Consumer>
                {
                    ({ addNewDisk, getPriceDisk }) => {
                        return (
                            < div className="add-disk" >
                                <div>
                                    <button onClick={() => this.props.history.push("/")}>Back to Panel</button>
                                    <div className="parameters">
                                        <label htmlFor="size">Size: </label>
                                        <select name="size" id="size" value={this.state.size} onChange={(e) => this.inputChange(e.target.name, e.target.value, getPriceDisk)}>
                                            <option value="2">2</option>
                                            <option value="8">8</option>
                                            <option value="16">16</option>
                                            <option value="32">32</option>
                                            <option value="64">64</option>
                                        </select>
                                        <label htmlFor="type">Type: </label>
                                        <select name="type" id="type" value={this.state.type} onChange={(e) => this.inputChange(e.target.name, e.target.value, getPriceDisk)}>
                                            <option value="HDD">HDD</option>
                                            <option value="SSD">SSD</option>
                                        </select>
                                        <p>Price: {this.state.priceDisk === 0 ? getPriceDisk(this.state.size, this.state.type) : this.state.priceDisk}$</p>
                                        <button onClick={() => this.addDiskButton(addNewDisk)}>+ ADD NEW DISK</button>
                                    </div>
                                </div>
                            </div>
                        )
                    }
                }
            </DiskContext.Consumer >
        )
    }
}

export default AddDisk;