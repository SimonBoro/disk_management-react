import React, { useContext, useState } from 'react';
import Disk from '../components/Disk';
import Users from '../components/Users';
import { DiskContext } from '../App';
import { Redirect } from 'react-router-dom';
import '../style/DiskPage.css';


const DiskPage = props => {
    const diskPage = true;
    const diskContext = useContext(DiskContext);
    let diskData = 0;
    let user = null;

    // const [email, setEmail] = useState("");
    // const [read, setRead] = useState(false);
    // const [write, setWrite] = useState(false);
    // const [del, setDel] = useState(false);

    const [form, setForm] = useState({
        email: '',
        read: false,
        write: false,
        del: false
    });

    const setEmail = email => setForm({
        ...form,
        email
    });

    const setRead = read => setForm({
        ...form,
        read
    });

    const setWrite = write => setForm({
        ...form,
        write
    });

    const setDel = del => setForm({
        ...form,
        del
    });

    const onChangeValue = (e) => {
        const { checked, name, value, type } = e.target

        if (type === 'text') {
            setForm({
                ...form,
                [name]: value
            })
        } else if (type === 'checkbox' || type === 'radio') {
            setForm({
                ...form,
                [name]: checked
            })
        }

        // if (name === "email") {
        //     setEmail(value)
        // } else if (name === "read") {
        //     setRead(checked)
        // } else if (name === "write") {
        //     setWrite(checked)
        // } else if (name === "del") {
        //     setDel(checked)
        // }
    }

    const addUser = () => {
        const add = diskContext.addNewUser(diskData.id, form.email, form.read, form.write, form.del);

        if (add) {
            setEmail("");
            setRead(false);
            setWrite(false);
            setDel(false);
        }

    }

    diskContext.disks.filter(disk => {
        if (disk.id === Number(props.match.params.id)) {
            diskData = disk
        }
        return diskData;
    })

    if (diskData.users !== undefined) {
        user = diskData.users.map(user => (
            <Users key={user.email} user={user} diskId={diskData.id} changeUserPermission={diskContext.changeUserPermission} deleteUser={diskContext.deleteUser} />
        ))
    }

    return (
        Number(props.match.params.id) === diskData.id ? (
            <div className="disk-page">
                <div className='disk-page-content'>
                    <button onClick={() => props.history.push("/")}>Back to Panel</button>
                    <Disk disk={diskData} diskPage={diskPage} deleteDisk={diskContext.deleteDisk} />
                    <table>
                        <caption>Users Permissions:</caption>
                        <thead>
                            <tr>
                                <td>User email</td>
                                <td>Read</td>
                                <td>Write</td>
                                <td>Delete</td>
                                <td>Add/Delete</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><input type="text" name="email" placeholder="email adress" value={form.email} onChange={onChangeValue} /></td>
                                <td><input type="checkbox" name="read" checked={form.read} onChange={onChangeValue} /></td>
                                <td><input type="checkbox" name="write" checked={form.write} onChange={onChangeValue} /></td>
                                <td><input type="checkbox" name="del" checked={form.del} onChange={onChangeValue} /></td>
                                <td><button onClick={addUser}>Add</button></td>
                            </tr>
                            {user}
                        </tbody>
                    </table>
                </div>
            </div>)
            : <Redirect to={"/"} />
    );
}

export default DiskPage;

/*

<PresentOrRedirect what={DiskPage} condition={props => props.match.params.id === diskData.id} redirectTarget="/" />

*/