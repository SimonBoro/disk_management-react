import React, { useContext } from 'react';
import { DiskContext } from '../App';
import Disk from '../components/Disk';
import '../style/Panel.css';

const Panel = (props) => {

    const diskPage = false;
    const diskContext = useContext(DiskContext);

    const disksPanel = diskContext.disks.map(disk => {
        return (
            <Disk key={disk.id} disk={disk} deleteDisk={diskContext.deleteDisk} diskPage={diskPage} props={props} />
        )
    })

    return (
        <div className="panel">
            <h1>REMOTE DISK MANAGEMENT</h1>
            <div>
                <button onClick={() => props.history.push("/add-disk")}>+ ADD DISK</button>
                {disksPanel}
            </div>
        </div>
    )
}

export default Panel;
